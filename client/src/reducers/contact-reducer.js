export default function contactReducer(state =[], action){
    const { type, payload } = action;
    switch(type){
        case "ADD_CONTACT":
            return [
                ...state,
                payload
            ];
        case "CLEAR_CONTACT":
            return(state=[]);
        default:
            return state;
    }
    return state;
};