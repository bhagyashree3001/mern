import React, { useEffect, useState} from "react";
import {connect } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import { contactAction, clearContact } from '../actions/contact-action';
import {Row,Button,Form,FormGroup,Input,Label,Col} from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import http from "../http-common";
import { Link } from "react-router-dom";
function ContactPage({contact,contactAction,clearContact}){
    //const contact=useSelector(state=>state.contact);
    //const dispatch = useDispatch();
    const [formData, setFormData] = useState({
        name: "",
        mobile: "",
        email: "",
        message : ""
    });
    const [errorList, setErrorList] = useState({});
    const [isSubmit, setIsSubmit] = useState(false);
    const { name, mobile, email, message } = formData;
    const [contacts , setContacts] = useState([]);
    const handleChange = e =>{
        setFormData({ ...formData, [e.target.name]: e.target.value });
    }

    useEffect(() => {
        getContactList();
    },[]);

    const getContactList = async() => {
        const res = await http.get(`/api/contact`);
        if(res.data.success==true){
            setContacts(res.data.data);
        }
    }
    const onSubmit=async e=>{
        e.preventDefault();
        setErrorList(validate(formData));
        if(Object.keys(errorList).length === 0 ){
            setIsSubmit(true);
            contactAction(formData);
            const res = await http.post(`/api/contact`, formData);
            if(res.data.success==true){
                alert(res.data.message);
                getContactList();
            }
        }
    }

    const deleteAllContact=async ()=>{
        clearContact([]);
        //dispatch(contactAction("CLEAR", []));
        if(window.confirm(`Are you sure you want to delete all contacts?`)){
            const res = await http.delete(`/api/contact`);
            if(res.data.success==true){
                alert(res.data.message);
                getContactList();
            }
        }
    }
    const deleteContact =async(id) =>{
        const res = await http.delete(`/api/contact/${id}`);
        if(res.data.success==true){
            alert(res.data.message);
            getContactList();
        }
    }

    const columns = [ {
        dataField: 'name',
        text: 'Name'
      }, {
        dataField: 'mobile',
        text: 'Mobile'
      }, {
        dataField: 'email',
        text: 'Email'
      }, {
        dataField: 'message',
        text: 'Message'
      },{
        dataField: "_id",
        text: "Action",
        formatter: (cellContent, row) => (
            <div>
                <Link to={`/edit-contact-us/${row._id}`}>
                <Button type="button" size="sm" color="success">Edit</Button> 
                </Link>
                <Button
                type="button"
                size="sm"
                color="danger"
                onClick={e => {
                    if(window.confirm(`Are you sure you want to delete : ${row.name}?`)){
                        deleteContact(row._id);
                    }
                }}
              >Delete</Button>
            </div>
          ),
      }];
    const validate = (values) =>{
        const errors ={}
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        const numberRegEx = /\-?\d*\.?\d{1,2}/;
        if(!values.name){
            errors.name= "Name is required";
        }
        if(!values.email){
            errors.email= "Email is required";
        }else if(!regex.test(values.email))
        {
            errors.email= "Email is not Correct";
        }
        if(!values.mobile){
            errors.mobile= "Mobile is required";
        } 
        else if(values.mobile.toString().length !== 10){
            errors.mobile= "Mobile should be 10 digit";
        } 
        if(!values.message){
            errors.message= "Message is required";
        }
        else if(!(values.message.length >20 && values.message.length <100)){
            errors.message= "Message should be between 20 to 100 words";
        }
        return errors;
    }
    return (
        <div><Row><Col xs="12" sm="12">
            <h4>Contact Us</h4>
                <Form className="form-horizontal" onSubmit={e => onSubmit(e)}>
                    <FormGroup>
                        <Label>Name : </Label>
                        <Input name="name" value ={formData.name || ''} onChange={e => handleChange(e)} />
                        {errorList.name && <p className="error">{errorList.name}</p>}
                    </FormGroup>
                    <FormGroup>
                        <Label> Email : </Label>
                        <Input name="email" type="text" value={formData.email} onChange={e => handleChange(e)} />
                        {errorList.email && <p className="error">{errorList.email}</p>}
                        
                    </FormGroup>
                    <FormGroup>
                        <Label>Mobile : </Label>
                        <Input  name="mobile" value ={formData.mobile} onChange={e => handleChange(e)} />
                        {errorList.mobile && <p className="error">{errorList.mobile}</p>}

                    </FormGroup>
                    <FormGroup>
                        <Label>Message : </Label>
                        <Input name="message" type="textarea" value={formData.message} onChange={e => handleChange(e)} />
                        {errorList.message && <p className="error">{errorList.message}</p>}

                    </FormGroup> 
                    <Button type="submit" className="m-1" size="sm" color="primary"> Submit
                    </Button>
                </Form>
                </Col>
                <Col xs="12" sm="12">
                    <h4>Contact List</h4>
                    <BootstrapTable keyField='_id' data={ contacts } columns={ columns } />
                    <Button onClick={deleteAllContact} className="m-1" size="md" color="danger"> Delete All
                    </Button>
                </Col>
            </Row>
        </div>
    )
}
function mapStateToProps(state){
    return {contact:state.contact}
}

export default connect(mapStateToProps,{contactAction,clearContact})(ContactPage);

