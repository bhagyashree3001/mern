import React, { useEffect, useState} from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Row,Button,Form,FormGroup,Input,Label,Col} from 'reactstrap';
import http from "../http-common";
import {useParams,useNavigate} from 'react-router-dom';
function EditContactPage({match}){
    let { id } = useParams();
    let navigate =useNavigate();
    const [formData, setFormData] = useState({
        name: "",
        mobile: "",
        email: "",
        message : ""
    });
    const [errorList, setErrorList] = useState({});
    const { name, mobile, email, message } = formData;
    const handleChange = e =>{
        setFormData({ ...formData, [e.target.name]: e.target.value });
    }
    useEffect(() => {
        getContactById();
    },[]);

    const getContactById = async() => {
        const res = await http.get(`/api/contact/${id}`);
        if(res.data.data!= null && res.data.success==true){
            setFormData({
                name: !res.data.data.name ? "" : res.data.data.name,
                email: !res.data.data.email ? "" : res.data.data.email,
                mobile: !res.data.data.mobile ? "" : res.data.data.mobile,
                message: !res.data.data.message ? "" : res.data.data.message,
            });
        }
    }
    const onSubmit=async e=>{
        e.preventDefault();
        const data = validate(formData);
        setErrorList(data);
        if(Object.keys(data).length === 0 ){
            const res = await http.post(`/api/contact/${id}`, formData);
            if(res.data.success==true){
                alert(res.data.message);
                navigate("/contact-us");
            }
        }
    }

    const validate = (values) =>{
        const errors ={};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        const numberRegEx = /\-?\d*\.?\d{1,2}/;
        if(!values.name){
            errors.name= "Name is required";
        }
        if(!values.email){
            errors.email= "Email is required";
        }else if(!regex.test(values.email)){
            errors.email= "Email is not Correct";
        }
        if(!values.mobile){
            errors.mobile= "Mobile is required";
        }else if(values.mobile && values.mobile.toString().length !== 10){
            errors.mobile= "Mobile should be 10 digits";
        }
        if(!values.message){
            errors.message= "Message is required";
        }else if(!(values.message.length >20 && values.message.length <100)){
            errors.message= "Message should be between 20 to 100 words";
        }
        return errors;
    }
    return (
        <div><Row><Col xs="12" sm="12">
            <h4>Contact Us</h4>
                <Form className="form-horizontal" onSubmit={e => onSubmit(e)}>
                    <FormGroup>
                        <Label>Name : </Label>
                        <Input name="name" value ={formData.name || ''} onChange={e => handleChange(e)} />
                        {errorList.name && <p className="error">{errorList.name}</p>}
                    </FormGroup>
                    <FormGroup>
                        <Label> Email : </Label>
                        <Input name="email" type="text" value={formData.email} onChange={e => handleChange(e)} />
                        {errorList.email && <p className="error">{errorList.email}</p>}
                        
                    </FormGroup>
                    <FormGroup>
                        <Label>Mobile : </Label>
                        <Input  name="mobile" value ={formData.mobile} onChange={e => handleChange(e)} />
                        {errorList.mobile && <p className="error">{errorList.mobile}</p>}

                    </FormGroup>
                    <FormGroup>
                        <Label>Message : </Label>
                        <Input name="message" type="textarea" value={formData.message} onChange={e => handleChange(e)} />
                        {errorList.message && <p className="error">{errorList.message}</p>}

                    </FormGroup> 
                    <Button type="submit" className="m-1" size="sm" color="primary"> Submit
                    </Button>
                </Form>
                </Col>
            </Row>
        </div>
    )
}

export default EditContactPage;

