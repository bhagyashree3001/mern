export const contactAction = (name) => dispatch => {
    dispatch({
      type: "ADD_CONTACT",
      payload: name 
    });
  };

export const clearContact = () => dispatch => {
  dispatch({ type: "CLEAR_CONTACT" });
};
