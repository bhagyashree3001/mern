// server.js
const express = require('express');
const app = express();
const PORT = 8000;
const cors = require("cors");
// Our DB Configuration
require('./src/database');

app.use(cors({
    'allowedHeaders': ['Content-Type'], // headers that React is sending to the API
    'exposedHeaders': ['Content-Type'], // headers that you are sending back to React
    'origin': '*',
    'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
    'preflightContinue': false
}));

// parse requests of content-type - application/json
app.use(express.json());

app.get('/', (req, res) => {
    res.send("Hello World ! ");
});

app.listen(PORT, function () {
    console.log(`Server Listening on ${PORT}`);
});

const bodyParser = require('body-parser');

app.use(
    bodyParser.urlencoded({
        extended: true
    })
    );
app.use(bodyParser.json());
    
// Routes
const contactRouter = require('./src/routes/Contact');
    
app.use('/api/contact', contactRouter);