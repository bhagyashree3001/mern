const mongoose = require("mongoose");

const ContactSchema = new mongoose.Schema({
  name:{
    type:String,
    required:true,
    maxlength:50
  },
  email: {
    type: String,
    required: true,
    unique: true,
    maxlength: 50
  },
  mobile:{
    type:Number,
    required:true,
  },
  message:{
    type:String,
    required:true,
    maxlength:100
  },
  created_at: {
    type: Number
  },
  updated_at: {
    type: Number
  }
},
{timestamps: {  createdAt : 'created_at' , updatedAt : 'updated_at'}
});

module.exports = Contact = mongoose.model("contact", ContactSchema);
