const express = require('express');
const contactRouter = express.Router();
const Contact = require('../models/Contact.model'); // post model

/* Get all Contact */
contactRouter.get('/', (req, res, next) => {
    Contact.find({} , function(err, result){
        if(err){
            res.status(400).send({
                'success': false,
                'error': err.message
            });
        }
        res.status(200).send({
            'success': true,
            'data': result
        });
    });
});


/* Add Single Contact */
contactRouter.post("/", (req, res, next) => {
    let newContact = {
        name: req.body.name,
        email:req.body.email,
        mobile: req.body.mobile,
        message:req.body.message
    };
    Contact.create(newContact, function(err, result) {
        if(err){
            res.status(400).send({
                success: false,
                error: err.message
            });
        }
        res.status(201).send({
            success: true,
            data: result,
            message: "Contact created successfully"
        });
    });
});

/* Delete Single Contact */
contactRouter.delete("/:contact_id?", (req, res, next) => {
    if(req.params.contact_id){
        Contact.findByIdAndDelete(req.params.contact_id, function(err, result){
            if(err){
              res.status(400).send({
                success: false,
                error: err.message
              });
            }
          res.status(200).send({
            success: true,
            data: result,
            message: "Contact deleted successfully"
          });
        });
    }
    else{
        Contact.deleteMany({}, function(err, result){
            if(err){
              res.status(400).send({
                success: false,
                error: err.message
              });
            }
          res.status(200).send({
            success: true,
            data: result,
            message: "All Contact deleted successfully"
          });
        });
    }
  });

/* Get Single Contact */
contactRouter.get("/:contact_id", (req, res, next) => {
    Contact.findById(req.params.contact_id, function (err, result) {
        if(err){
             res.status(400).send({
               success: false,
               error: err.message
             });
        }
        res.status(200).send({
            success: true,
            data: result
        });
     });
});

/* Updayeingle Contact */
contactRouter.post("/:contact_id", (req, res, next) => {
    let updateContact = {
        name: req.body.name,
        email:req.body.email,
        mobile: req.body.mobile,
        message:req.body.message
    };
    Contact.findByIdAndUpdate(req.params.contact_id,{$set: updateContact}, {new:true}, function(err, result) {
        if(err){
            res.status(400).send({
                success: false,
                error: err.message
            });
        }
        res.status(201).send({
            success: true,
            data: result,
            message: "Contact Updated successfully"
        });
    });

});
module.exports = contactRouter;